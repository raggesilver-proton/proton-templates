#!/bin/bash

# All template scripts need to cd into their own directory
cd "$( dirname "${BASH_SOURCE[0]}" )"

sed -i "s/@PROJECT_NAME@/$1/g" meson.build.in

mv meson.build.in meson.build

# Template scripts should delete themselves
rm $0
