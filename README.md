<div align="center">
    <h1>
        Templates
    </h1>
    <h4>Project templates for Proton</h4>
    <p>
        <!-- <a href="https://gitlab.com/raggesilver-proton/proton/pipelines">
            <img src="https://gitlab.com/raggesilver-proton/proton/badges/master/pipeline.svg" alt="Build Status" />
        </a> -->
        <a href="https://www.patreon.com/raggesilver">
            <img src="https://img.shields.io/badge/patreon-donate-orange.svg?logo=patreon" alt="Proton on Patreon" />
        </a>
    </p>
    <!-- <p>
        <a href="#install">Install</a> •
        <a href="#features">Features</a> •
        <a href="https://gitlab.com/raggesilver-proton/proton/blob/master/COPYING">License</a>
    </p> -->
</div>

Proton can create a new project based on template. This repo contains the
standard templates shipped with Proton.

Most templates have a `.proton.json` on the project root which integrates with
[proton-runner-plugin](https://gitlab.com/raggesilver-proton/proton-runner-plugin).

**Important notice**: templates might have `template_setup.sh` at the root which
will be executed after the template files are copied, you should be careful if
you get any third party templates. It's also important to point out that none of
Proton's templates come with any warranty.

## Making your own templates

Use [templates/c_meson](templates/c_meson) as base for your new
template (it has useful comments in `template_setup.sh`).

The easiest way to add the new template is to go to the "New Project" section
on Proton's welcome window and click "Open templates" to go the installed folder.

> You should **not** modify the existing templates as that may cause update
issues.
